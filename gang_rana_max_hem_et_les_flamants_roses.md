
# La clairière

Il était une fois un petit hérisson qui mangeait des limaces dans la clairière. Il
y avait beaucoup de limaces cet automne parce qu'il avait beaucoup plu. Gang
mangeait ces limaces sous la pluie parce qu'il savait que ce serait bientôt
l'hiver et qu'il fallait prendre des réserves et qu'il avait encore très faim.
Ce qu'il ne savait pas c'est que derrière un buisson, quelqu'un d'autre avait
très faim. Le renard le guettait depuis un moment. Lorsque le hérisson fut assez
proche le renard sauta dessus. Gang se mis tout de suite en boule. Ses pics le
protégeaient bien. Le renard attendit un moment sans bouger. Puis il utilisa sa
ruse bien connue: il fit pipi sur le dos du hérisson. Gang, croyait que c'était
la pluie qui tombait sur son dos, et par réflexe, il se détendit. Le renard en
profita pour le retourner avec sa patte. Il voyait maintenant le ventre du
hérisson. Ca y est, il allait le manger.

A ce moment un grognement se fit sentir. Un ours énorme levait les bras au ciel
et montrait toute sa force d'ours. Le renard fila, la queue entre les jambes.
L'ours s'appelait Max et c'était l'ami de Gang. "Oh merci merci" - dit Gang -
"Portes moi dans tes bras s'il te plaît". Max pris Gang dans ses bras et le
petit hérisson l'embrassa bien fort. Gang promis à max qu'il irait lui chercher
des petites baies sous les buissons touffus. Il savait bien ramasser les baies
dans les endroits tous serrés où Max ne pouvait pas passer. 

Gang aimait faire de la balançoire avec son amie Rana, une lapine.
C'est Max qui leur avait construite cette balançoire avec un grand baton posé
sur un rondin de bois. Ils passaient de bon moment comme ça dans la clairière.
Ils aimaient se raconter des histoires et parler de voyages. Un jour Max leur
avait parlé des flamants roses qui vivaient en Camargue. Gang et Rana
l'avaient écouté avec des étoiles dans les yeux. Ce serait tellement bien de
voir les flamants roses en vrai. Alors ils décidèrent de partir en voyage. 

# Le voyage en train

L'ours savait lire les étiquettes sur les train. Il trouva un train de
marchandises qui partait
en direction de Marseille. Ce train ne 
L'ours ne devait pas dépasser du train ils étaient dans un trou entre les
troncs. 
Le train ne bougeait pas depuis un moment.  



