# Hippo et Coco 

*Notes de lecture: Hippo et ses parents parlent avec la lèvre inférieure relevée sur les
dents du haut. Coco a une voix de perroquet cela va sans dire.* 

Un hippopotame se promenait tranquillement dans la rivière. Il ne touchait pas le fond
et il nageait. Son papa et sa maman se prélassaient au bord de l'eau, sur un banc de
sable. Ils demandèrent si Hippo pouvait leur amener des algues à manger. Hippo se fit un
plaisir de saisir les algues avec ses dents au milieux de la rivière. Il en arrachait à
la surface, mais elles se déchiraient en petit morceaux. Alors il devait plonger pour
aller en saisir la racine. Et il amenait tout fier, un gros paquet d'algues à son père.
Il savait que sa maman les trouvaient trop amères. Elle lui avait dit "Tu sais Hippo, il
y a ses algues douces, comme de la salade là bas, en amont de la rivière". Ah que ne
ferait on pas pour sa mère! Le petit hippopotame partit faire un voyage à
contre-courant. Il fallait nager fort au début, mais bientôt ses pieds touchèrent terre
et il pu simplement marcher dans le lit de la rivière. Il passa un embranchement et
continua sa marche vers le haut. C'était maintenant un petit ruisseau. Il pouvait voir
loin devant les petites algues vert tendre qui plaisaient tant à sa maman. Il marcha
vers elles; en mordit plusieurs bouchées par la racine. A peine avait-il commencé sa
marche dans l'autre sens qu'un orage éclata. Une pluie torrentielle se déversait tout
autour sur le paysage. 

Le ruisseau se mit à gonfler, gonfler. Hippo devait maintenant nager là ou il pouvait
marcher auparavant. Le courant devenait de plus en plus fort. Il s'approchait de
l'embranchement et essayait de nager vers la gauche pour rejoindre ses parents, mais le
courant était plus fort et l'emportait vers la droite. Ah non! Il se dirigeait vers la
mer. Il avançait très vite maintenant, les arbres défilaient devant ses yeux. Il avait
toujours les algues vert tendre dans la bouche. S'il les avaient lâchés il aurait pu
s'agripper à une branche avec ses dents. Coco le perroquet observait la scène depuis son
arbre perché. Il lui cria: "lâche les algues! Si tu continues, tu vas arriver à la mer.
Tu sais bien que les hippopotames vivent dans l'eau douce. Ils ne supportent pas l'eau
salée." Par trois fois coco cria encore "lâche les algues Hippo! Lâche les algues! Lâche
les algues!". Il les lâchât et pu finalement mordre une grosse branche qui dépassait. Il
se laissa pousser vers des cailloux plats et de l'eau peu profonde. Il était épuisé, sur
un rivage inconnu.

Coco le perroquet lui demanda d'où il venait. "Je vis dans l'autre branche de la rivière
là bas plus haut. Mes parents se prélassent souvent sur un gros rocher blanc près d'un
banc de sable." Coco ne connaissait pas ce rocher blanc. Il alla voir son frère qui
voyageait beaucoup et lui demanda s'il avait déjà vu le banc de sable sur l'autre branche
de la rivière. "Oui je l'ai vu. Et j'y ai vu aussi les hippopotames endormis". Hippo
était trop petit pour remonter le courant fort. "Comment faire pour l'aider ?" pensait
coco. Est-ce qu'on pouvait le tirer d'une manière ou d'une autre? Hippo ne pouvait pas
non plus marcher si longtemps sur le rivage. De tout façon la forêt était trop dense,
pleine de branches entremêlées et de lianes. "Ah c'est ça! On pourrait le tirer avec une
liane!" s'exclama coco. Hippo était petit, mais il pesait déjà 300kg et les deux frères
perroquets n'avaient pas assez de force pour le déplacer.

Coco vola vers ses amis les cockatoo. Très nombreux ils hurlaient comme d'habitude
perchés sur un grand arbre. Il appela très fort pour se faire entendre à travers leurs
hurlements. "Venez m'aider s'il vous plaît! J'ai besoin de tirer un hippopotame avec une
corde!" Peu de temps après, une centaine de cockatoo s'accrochèrent à la plus grande
liane qu'ils aient pu trouver. Ils s'approchèrent de notre ami pour qu'il puisse saisir
la liane dans sa bouche. Et on vit s'élever les 100 oiseaux blancs à crêtes jaune qui se
mirent à tirer l'hippopotame. Jamais on n'avait vu si beau spectacle! Ils faisaient
tourner les têtes des spectateurs nombreux sur le rivage. Le magnifique attelage remonta
la rivière jusqu'à l'embranchement. Hippo leur fit comprendre qu'ils voulait retourner
chercher des algues salades en amont de la rivière. Alors les oiseaux le tirèrent encore
jusqu'à ce qu'il ait pied. Puis ils lâchèrent la corde et firent un grand cercle d'au
revoir. Hippo remonta encore les pieds dans le ruisseau jusqu'aux algues tendres, en
pris quelques bouché puis redescendit le ruisseau les porter à sa mère. Arrivé à l'embranchement le
courant avait suffisamment baissé pour qu'il puisse nager vers la gauche et il avait
bientôt rejoint ses parents. Papa Hippo et maman hippo s'étaient beaucoup inquiétés. "Où
étais-tu? Nous t'avons appelé toute l'après-midi sous la pluie!". Il leur raconta toute
l'histoire avant de s'endormir près d'eux.



Références: Poids d'un bébé hippopotame https://zoodegranby.com/fr/bebe-hippopotame
Combien peut porter un aigle ou autre rapace
http://art-et-culture-du-monde.fr/blog/2018/08/combien-peut-porter-un-aigle-ou-un-autre-rapace/

