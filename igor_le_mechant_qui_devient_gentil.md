
Il était une fois Igor, un enfant qui était très pauvre. 
Il allait devenir méchant à cause d'une injustice. 

- C'est quoi une injustice? 

Sa maman ne voulait pas lui donner à manger. 
La première fois il est parti chez les voisin pour leur demander à manger. 
Les voisins ont partagé un peu de leur repas avec lui. La deuxième fois aussi, 
mais la troisième fois, les voisin qui n'étaient pas si généreux lui ont dit:
"Non! Nous ne te donnerons plus à manger, débrouille toi tout seul!" 

Alors Igor est parti dans la rue. Puis il est parti au magasin. Il a volé un
morceau de pain et des pommes. 
Le propriétaire du magasin c'est énervé, il n'était pas content qu'on lui ait
volé du pain et des pommes. 
De fil en aiguille, le garçon est devenu de plus en plus méchant. Il était
persuadé que de toute façon, le monde était injuste. Alors autant faire le mal,
tout le temps et voler et tapper sans arrêt. 
Jusqu'au jour où il a fini en prison. 

En prison, il avait une fille qui faisait découvrir les livres aux prisonniers.
Cette fille lui a appris à lire la musique dans les livres. Et Igor s'est pris
de passion pour la flûte traversière. 
Lorsqu'après de longues années, il est enfin sorti de prison. Il s'est 
mis à jouer de la flûte traversière dans la rue. 
Les gens qui passaient par là, lui ont jeté des pièces. Il était très surpris:
"Comment? Pourquoi ces gens sont-ils généreux avec moi?" 

Peu de temps après, un guitariste vint à sa rencontre. "Est-ce que je peux
jouer avec toi?". "Bien sûr", lui dit Igor, "comment t'appelles tu?". "Je
m'appelle José". Il se mit à l'accompagner à la guitare. Ils jouèrent ensemble
dans des salles de musique, puis dans des salles de concert. La vie d'Igor avait
changé. 


